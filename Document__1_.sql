
SQL> create table adminlogin(
    admin_name varchar2(20),
    admin_username varchar2(20) unique,
    admin_password varchar2(8) unique);

Table created.

SQL> desc adminlogin;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 ADMIN_NAME                                             VARCHAR2(20)
 ADMIN_USERNAME                                         VARCHAR2(20)
 ADMIN_PASSWORD                                         VARCHAR2(8)


SQL> create table passenger(
    passenger_id number(5) unique,
    passenger_name varchar2(20),
    passenger_address varchar2(20),
    phone varchar2(10),
    email varchar2(20));

Table created.

SQL> desc passenger;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 PASSENGER_ID                                      NOT NULL NUMBER
 PASSENGER_NAME                                             VARCHAR2(20)
 PASSENGER_ADDRESS                                          VARCHAR2(50)
 PHONE                                              VARCHAR2(10)
 PASSENEGR_EMAIL                                            VARCHAR2(50)



SQL> create table passengerlogin (
   username varchar2(20) unique,
    password varchar2(8) unique);

Table created.

SQL> desc passengerlogin;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
USERNAME                                           VARCHAR2(20)
 PASSWORD                                           VARCHAR2(8)


SQL> create table bus(
    bus_id number(5) unique,
    b_number varchar2(8) unique,
    b_name varchar2(10));

Table created.

SQL> desc bus;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 B_ID                                      NOT NULL NUMBER
 B_NUMBER                                           VARCHAR2(12)
 B_NAME                                             VARCHAR2(50)

 SQL> Create table BusOperator(
    operator_id number primary key,
    operator_name varchar2(25) not null,
    phoneNumber varchar2(10) not null,
    licenseNumber varcahr2(10) unique,
    mail varchar2(30) unique);

Table created.

SQL> desc BusOperator;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 OPERATOR_ID                               NOT NULL NUMBER
 OPERATOR_NAME                             NOT NULL VARCHAR2(25)
 PHONENUMBER                               NOT NULL VARCHAR2(10)
 LICENSENUMBER                                      VARCHAR2(10)
 MAIL                                               VARCHAR2(30)

 SQL> create table Operatorlogin (
    username varchar2(20) unique,
    password varchar2(8) unique);

Table created.

SQL> desc OperatorLogin;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 USERNAME                                           VARCHAR2(20)
 PASSWORD                                           VARCHAR2(8)




